from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('languages', views.LanguageView)
urlpatterns = [
   # path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
   # path('', include('languages.urls'))
    path('', include(router.urls))
]